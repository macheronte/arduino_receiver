/*
Reference IR: http://www.circuitbasics.com/arduino-ir-remote-receiver-tutorial/
Reference RF: https://github.com/sui77/rc-switch/ (ReceiveDemo Advanced)
*/

#include <IRremote.h>
#include <RCSwitch.h>

const int RECV_PIN_IR = 4;
const int RECV_PIN_RF = 2;

IRrecv irrecv(RECV_PIN_IR);
RCSwitch mySwitch = RCSwitch();

decode_results results;

void setup(){
  Serial.begin(9600);
  
  irrecv.enableIRIn();
  irrecv.blink13(true);
  mySwitch.enableReceive(RECV_PIN_RF - 2);  // Receiver on interrupt 0 => that is pin #2
}

void loop(){
  if (irrecv.decode(&results)){
        Serial.print("IR Code: ");
        Serial.println(results.value, HEX);
        irrecv.resume();
  }

  if (mySwitch.available()) {
    Serial.println("RF Code --------------------------------------------------------------------------------------------------------------------------------------");
    output(mySwitch.getReceivedValue(), mySwitch.getReceivedBitlength(), mySwitch.getReceivedDelay(), mySwitch.getReceivedRawdata(),mySwitch.getReceivedProtocol());
    Serial.println("-----------------------------------------------------------------------------------------------------------------------------------------------");
    mySwitch.resetAvailable();
  }
}
